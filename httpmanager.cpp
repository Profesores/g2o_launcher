#include "httpmanager.h"

HttpManager::HttpManager(QObject *parent) : QObject(parent)
{
    connect(&m_Manager, SIGNAL(finished(QNetworkReply*)), SLOT(onRequestFinished(QNetworkReply*)));
}

void HttpManager::post(QUrl url, QJsonDocument doc)
{
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    m_Manager.post(request, doc.toJson());
}

void HttpManager::get(QUrl url)
{
    QNetworkRequest request(url);
    m_Manager.get(request);
}

void HttpManager::onRequestFinished(QNetworkReply *pReply)
{
    QJsonDocument response = QJsonDocument::fromJson(pReply->readAll());

    emit finished(pReply->url(), response);
    pReply->deleteLater();
}
