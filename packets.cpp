#include "packets.h"

PacketPing PacketPing::parse(ServerAddress &address, char *buffer, int len)
{
    Q_UNUSED(len);
    Q_UNUSED(buffer);

    PacketPing packet;
    packet.address = address;

    return packet;
}

PacketInfo PacketInfo::parse(ServerAddress &address, char *buffer, int len)
{
    uchar version = buffer[3];

    if (version == PACKET_VERSION)
    {
        PacketInfo packet;
        packet.address = address;

        // Server version
        packet.version.major = buffer[4];
        packet.version.minor = buffer[5];
        packet.version.patch = buffer[6];

        // Slots
        packet.players = static_cast<unsigned char>(buffer[7]);
        packet.maxSlots = static_cast<unsigned char>(buffer[8]);

        // Read Host name
        char sHostName[34] = { 0 };
        memcpy(sHostName, &buffer[9], len - 9);
        packet.hostName = sHostName;

        return packet;
    }

    return PacketInfo();
}

PacketInfo PacketInfo::empty(ServerAddress &address)
{
    PacketInfo packet;
    packet.address = address;

    // Server version
    packet.version.major = 0;
    packet.version.minor = 0;
    packet.version.patch = 0;

    // Info
    packet.players = 0;
    packet.maxSlots = 0;
    packet.hostName = "Unknown hostname";
    packet.ping = 9999;

    return packet;
}

PacketDetails PacketDetails::parse(ServerAddress &address, char *buffer, int len)
{
    uchar version = buffer[3];

    if (version == PACKET_VERSION)
    {
        PacketDetails packet;
        packet.address = address;

        // Read world
        int worldLen = buffer[4];
        char sWorld[34] = { 0 };
        memcpy(sWorld, &buffer[5], worldLen);
        packet.world = sWorld;

        // Read description
        char sDescription[410] = { 0 };
        memcpy(sDescription, &buffer[5 + worldLen], len - 5 - worldLen);
        packet.description = sDescription;

        return packet;
    }

    return PacketDetails();
}
