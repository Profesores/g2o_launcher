#include <QProcess>
#include <QDir>
#include <QFileInfo>
#include <QVector>
#include <QCoreApplication>

#include "updatescript.h"

UpdateScript::UpdateScript(QStringList files)
{
    QFile script;
    script.setFileName("update.bat");
    if (!script.open(QIODevice::WriteOnly))
        return;

    script.write("@ECHO OFF\n");
    script.write("TIMEOUT /t 1 /nobreak > NUL\n");
    script.write("TASKKILL /IM \"G2O_Launcher.exe\" > NUL\n");

    QString mainDir = QDir::currentPath();
    QString lastDir = mainDir;

    for (auto file : files)
    {
        QFileInfo fileInfo(file);
        if (fileInfo.isDir())
        {
            lastDir = fileInfo.absoluteFilePath();
            script.write(QString("CD \"%1\"\n").arg(lastDir).toLatin1());
        }
        else
        {
            if (fileInfo.absolutePath() == lastDir)
                script.write(QString("MOVE \"%1\" \"%2\"\n").arg(fileInfo.fileName() + ".update").arg(fileInfo.fileName()).toLatin1());
            else
            {
                lastDir = fileInfo.absolutePath();

                script.write(QString("CD \"%1\"\n").arg(lastDir).toLatin1());
                script.write(QString("MOVE \"%1\" \"%2\"\n").arg(fileInfo.fileName() + ".update").arg(fileInfo.fileName()).toLatin1());
            }
        }
    }

    // Go back to the main directory if needed
    if (lastDir != mainDir)
        script.write(QString("CD \"%1\"\n").arg(mainDir).toLatin1());

    script.write("DEL \"%~f0\" & START \"\" /B \"G2O_Launcher.exe\"");
    script.close();
}

void UpdateScript::execute()
{
    QProcess process;
    process.startDetached("cmd.exe", QStringList() << "/c" << "update.bat");

    QCoreApplication::quit();
}
