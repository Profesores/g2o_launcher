#ifndef UPDATESCRIPT_H
#define UPDATESCRIPT_H

#include <QStringList>
#include <QFile>

class UpdateScript
{
public:
    UpdateScript(QStringList files);

    void execute();
};

#endif // UPDATESCRIPT_H
