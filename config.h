#ifndef CONFIG_H
#define CONFIG_H

#include <QSettings>
#include <QString>

#include "serveraddress.h"

class Config
{
public:
    Config();

    static Config& get();
    void setNickname(QString nickname);
    void setWorld(QString world);
    void setAddress(ServerAddress address);
    void setLang(QString lang);
    QString getNickname();
    QString getWorld();
    ServerAddress getAddress();
    QString getLang();

private:
    QSettings m_Registry;
};

#endif // CONFIG_H
