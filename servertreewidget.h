#ifndef SERVERTREEWIDGET_H
#define SERVERTREEWIDGET_H

#include <QTreeWidget>
#include <QVector>
#include <QThread>

#include "serveraddress.h"
#include "servermanager.h"

class ServerTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    explicit ServerTreeWidget(QWidget *parent = 0);
    void init();
    void insert(PacketInfo &packet);
};

#endif // SERVERTREEWIDGET_H
