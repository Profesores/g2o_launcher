#ifndef SERVERLIST_H
#define SERVERLIST_H

#include <QObject>
#include <QVector>
#include <QTimer>
#include <QPoint>
#include <QAction>
#include <QMenu>
#include <QTreeWidgetItem>

#include "serveraddress.h"
#include "servermanager.h"

namespace Ui {
class Launcher;
}

class ServerList : public QObject
{
    Q_OBJECT

public:
    explicit ServerList(Ui::Launcher *ui, QObject *parent = nullptr);
    ~ServerList();

    virtual void refresh(QVector<ServerAddress> addresses);

protected slots:
    virtual void onTimeout();
    virtual void onServerInfo(PacketInfo packet);
    virtual void onServerDetails(PacketDetails packet);
    virtual void onServerClicked(QTreeWidgetItem *pItem, int id);
    virtual void onServerDoubleClicked(QTreeWidgetItem *pItem, int id);
    virtual void onContexMenu(QPoint point);

protected:
    void join(ServerAddress address, Version version);

    ServerManager m_Manager;
    QThread m_Thread;
    QTimer *m_pTimer;
    Ui::Launcher *pUi;
    QString m_World;
};

#endif // SERVERLIST_H
