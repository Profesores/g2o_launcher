#include <QDomDocument>
#include <QDomNode>
#include <QDomElement>
#include <QTextStream>
#include <QInputDialog>
#include <QMessageBox>
#include <QFile>

#include "favoritelist.h"
#include "ui_launcher.h"

FavoriteList::FavoriteList(Ui::Launcher *ui) :
    ServerList(ui)
{
}

FavoriteList::~FavoriteList()
{
    save();
}

void FavoriteList::init()
{
    // Remove margins so it looks much better
    pUi->tabFavorite->layout()->setMargin(0);
    pUi->treeFavorite->init();

    // Connections
    connect(&m_Manager, SIGNAL(serverInfo(PacketInfo)), SLOT(onServerInfo(PacketInfo)));
    connect(&m_Manager, SIGNAL(serverDetails(PacketDetails)), SLOT(onServerDetails(PacketDetails)));
    connect(pUi->treeFavorite, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(onServerClicked(QTreeWidgetItem*,int)));
    connect(pUi->treeFavorite, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(onServerDoubleClicked(QTreeWidgetItem*,int)));
    connect(pUi->btnFavAdd, SIGNAL(released()), SLOT(onAdd()));
    connect(pUi->btnFavEdit, SIGNAL(released()), SLOT(onEdit()));
    connect(pUi->btnFavRemove, SIGNAL(released()), SLOT(onRemove()));
    connect(m_pTimer, SIGNAL(timeout()), SLOT(onTimeout()));

    load();
}

void FavoriteList::refresh()
{
    pUi->treeFavorite->clear();
    ServerList::refresh(m_Servers);
}

void FavoriteList::load()
{
    QDomDocument doc;
    QFile file("favorite.xml");
    if (!file.open(QIODevice::ReadOnly) || !doc.setContent(&file))
        return;

    QDomNodeList servers = doc.elementsByTagName("server");
    for (int i = 0; i < servers.size(); ++i)
    {
        QDomNode server = servers.item(i);
        QDomElement ipAddr = server.firstChildElement("ip");
        QDomElement port = server.firstChildElement("port");

        if (ipAddr.isNull() || port.isNull())
            continue;

        m_Servers.append({ipAddr.text(), port.text().toUShort()});
    }
}

void FavoriteList::save()
{
    QFile file("favorite.xml");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QDomDocument doc;
    QDomElement servers = doc.createElement("servers");

    for (auto& address : m_Servers)
    {
        QDomElement ipAddr = doc.createElement("ip");
        ipAddr.appendChild(doc.createTextNode(address.IP));
        QDomElement port = doc.createElement("port");
        port.appendChild(doc.createTextNode(QString::number(address.Port)));

        QDomElement server = doc.createElement("server");
        server.appendChild(ipAddr);
        server.appendChild(port);

        servers.appendChild(server);
    }

    doc.appendChild(servers);

    QTextStream outStream(&file);
    outStream << doc.toString();
    file.close();
}

void FavoriteList::onTimeout()
{
    ServerList::onTimeout();

    // Fill servers that didn't respond
    QVector<ServerAddress> addresses = m_Manager.failRequests();
    for (auto address : addresses)
        pUi->treeFavorite->insert(PacketInfo::empty(address));
}

void FavoriteList::onServerInfo(PacketInfo packet)
{
    pUi->treeFavorite->insert(packet);
}

void FavoriteList::onAdd()
{
    QString ipAddr = QInputDialog::getText(nullptr, "Add server", "Enter IP:Port");
    ServerAddress address = ServerAddress::parse(ipAddr);
    if (address.isUnassigned())
    {
        QMessageBox::warning(nullptr, "Favorite", "Invalid IP:Port!");
        return;
    }

    m_Servers.append(address);
    refresh();
}

void FavoriteList::onEdit()
{
    auto pItem = pUi->treeFavorite->currentItem();
    if (pItem == nullptr) return;

    QString ip = pItem->text(1);
    ushort port = pItem->text(2).toUShort();

    for (auto& srv : m_Servers)
    {
        if (srv.IP == ip && srv.Port == port)
        {
            QString ipAddr = ip + ":" + QString::number(port);
            ipAddr = QInputDialog::getText(nullptr, "Edit server", "Enter IP:Port", QLineEdit::Normal, ipAddr);

            ServerAddress address = ServerAddress::parse(ipAddr);
            if (address.isUnassigned())
            {
                QMessageBox::warning(nullptr, "Favorite", "Invalid IP:Port!");
                return;
            }

            srv = address;
            refresh();

            return;
        }
    }
}

void FavoriteList::onRemove()
{
    auto pItem = pUi->treeFavorite->currentItem();
    if (pItem == nullptr) return;

    QString ip = pItem->text(1);
    ushort port = pItem->text(2).toUShort();

    int idx = 0;
    for (auto& srv : m_Servers)
    {
        if (srv.IP == ip && srv.Port == port)
        {
            m_Servers[idx] = m_Servers.back();
            m_Servers.pop_back();

            refresh();
            return;
        }

        ++idx;
    }
}
