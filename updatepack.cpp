#include <QBuffer>
#include <QIODevice>
#include <QDir>
#include <JlCompress.h>

#include "updatepack.h"

UpdatePack::UpdatePack(QByteArray data)
{
    // Buffer in memory
    if (!m_Buffer.open(QIODevice::WriteOnly))
        return;

    m_Buffer.write(data);
    m_Buffer.close();
}

void UpdatePack::extract()
{
    QuaZip zipFile(&m_Buffer);
    if (!zipFile.open(QuaZip::mdUnzip))
        return; // Cannon open to unzip

    if (!zipFile.goToFirstFile())
        return; // No files

    do
    {
        QString filePath = zipFile.getCurrentFileName();
        createFile(zipFile, filePath);
    } while (zipFile.goToNextFile());

    zipFile.close();
    if (zipFile.getZipError() != 0)
    {
        // ERROR
    }
}

QStringList UpdatePack::files()
{
    QuaZip zipFile(&m_Buffer);
    if (!zipFile.open(QuaZip::mdUnzip))
        return QStringList();

    if (!zipFile.goToFirstFile())
        return QStringList();

    QStringList listFiles;

    do
    {
        QString filePath = zipFile.getCurrentFileName();
        listFiles.append(filePath);

    } while (zipFile.goToNextFile());

    zipFile.close();
    return listFiles;
}

bool UpdatePack::createFile(QuaZip &zipFile, QString filePath)
{
    QuaZipFile inFile(&zipFile);
    if (!inFile.open(QIODevice::ReadOnly) || inFile.getZipError() != UNZ_OK)
        return false;

    // Create dirpath if not exist
    if (filePath.at(filePath.size() - 1) == '/')
    {
        QDir dir(filePath);
        if (!dir.exists())
            dir.mkpath(".");

        return true;
    }

    filePath += ".update";

    QFile outFile;
    outFile.setFileName(filePath);

    if (!outFile.open(QIODevice::WriteOnly))
        return false;

    if (!copyData(inFile, outFile) || inFile.getZipError() != UNZ_OK)
    {
        outFile.close();
        return false;
    }

    outFile.close();
    inFile.close();

    return inFile.getZipError() == UNZ_OK;
}

bool UpdatePack::copyData(QIODevice &inFile, QIODevice &outFile)
{
    while (!inFile.atEnd()) {
        char buf[4096];
        qint64 readLen = inFile.read(buf, 4096);
        if (readLen <= 0)
            return false;
        if (outFile.write(buf, readLen) != readLen)
            return false;
    }
    return true;
}
