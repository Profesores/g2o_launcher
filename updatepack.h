#ifndef UPDATEPACK_H
#define UPDATEPACK_H

#include <QBuffer>
#include <quazip.h>

class UpdatePack
{
public:
    UpdatePack(QByteArray data);

    void extract();
    QStringList files();

private:
    bool createFile(QuaZip &zipFile, QString filePath);
    bool copyData(QIODevice &inFile, QIODevice &outFile);

    QBuffer m_Buffer;
};

#endif // UPDATEPACK_H
