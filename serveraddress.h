#ifndef SERVERADDRESS_H
#define SERVERADDRESS_H

#include <QString>

class ServerAddress
{
public:
    static ServerAddress parse(QString ipAddr);
    bool isUnassigned();

    QString IP;
    ushort Port;
};

#endif // SERVERADDRESS_H
