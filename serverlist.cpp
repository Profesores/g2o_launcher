#include <QMessageBox>

#include "serverlist.h"
#include "proxy.h"
#include "config.h"
#include "ui_launcher.h"

ServerList::ServerList(Ui::Launcher *ui, QObject *parent) :
    QObject(parent),
    pUi(ui)
{
    m_Manager.start(m_Thread);

    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(3000);
    m_pTimer->setSingleShot(true);
}

ServerList::~ServerList()
{
    m_Manager.stop();
    m_Thread.quit();
    m_Thread.wait();
    m_Thread.deleteLater();
}

void ServerList::refresh(QVector<ServerAddress> addresses)
{
    m_Manager.clear();
    for (auto& address : addresses)
        m_Manager.info(address);

    pUi->btnRefresh->setEnabled(false);
    m_pTimer->start();
}

void ServerList::join(ServerAddress address, Version version)
{
    if (version.major == 0 && version.minor == 0 && version.patch == 0)
    {
        QMessageBox::warning(nullptr, "Server", "This server is offline!");
        return;
    }

    // Save info to the registry
    Config::get().setNickname(pUi->editNickname->text());
    Config::get().setAddress(address);
    Config::get().setWorld(m_World);

    Proxy::run(version.major,
               version.minor,
               version.patch);
}

void ServerList::onTimeout()
{
    pUi->btnRefresh->setEnabled(true);
}

void ServerList::onServerInfo(PacketInfo packet)
{
}

void ServerList::onServerDetails(PacketDetails packet)
{
    m_World = packet.world;

    pUi->viewWorld->setText("<b>World:</b> " + packet.world);
    pUi->viewDescription->setText(packet.description);
}

void ServerList::onServerClicked(QTreeWidgetItem *pItem, int id)
{
    Q_UNUSED(id);
    m_Manager.details({pItem->text(1), pItem->text(2).toUShort()});
}

void ServerList::onServerDoubleClicked(QTreeWidgetItem *pItem, int id)
{
    Q_UNUSED(id);

    QStringList versionSplited = pItem->text(3).split('.');
    if (versionSplited.size() != 3) // Something is wrong with version
        return;

    Version version;
    version.major = versionSplited.at(0).toInt();
    version.minor = versionSplited.at(1).toInt();
    version.patch = versionSplited.at(2).toInt();

    join({ pItem->text(1), pItem->text(2).toUShort() }, version);
}

void ServerList::onContexMenu(QPoint point)
{
    Q_UNUSED(point);
}
