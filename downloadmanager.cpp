#include "downloadmanager.h"

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{
    connect(&m_Manager, SIGNAL(finished(QNetworkReply*)), SLOT(onDownloadFinished(QNetworkReply*)));
}

void DownloadManager::download(QUrl url)
{
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

    QNetworkReply *pReply = m_Manager.get(request);
    connect(pReply, SIGNAL(downloadProgress(qint64,qint64)), SLOT(onDownloadProgress(qint64,qint64)));

    m_Downloads.append(pReply);
}

void DownloadManager::onDownloadFinished(QNetworkReply *pReply)
{
    QUrl url = pReply->url();
    if (pReply->error())
    {
        qDebug() << "Download failed: " << pReply->errorString();
        return;
    }

    emit finished(pReply->url(), pReply->readAll());

    m_Downloads.removeAll(pReply);
    pReply->deleteLater();
}

void DownloadManager::onDownloadProgress(qint64 current, qint64 max)
{
    if (current % 16 == 0 || current == max)
        emit progress(current, max);
}
