#ifndef FAVORITELIST_H
#define FAVORITELIST_H

#include "serverlist.h"

class FavoriteList : public ServerList
{
    Q_OBJECT

public:
    FavoriteList(Ui::Launcher *ui);
    ~FavoriteList();
    void init();
    void refresh();

private slots:
    void onTimeout() override;
    void onServerInfo(PacketInfo packet) override;
    void onAdd();
    void onEdit();
    void onRemove();

private:
    void load();
    void save();

    QVector<ServerAddress> m_Servers;
};

#endif // FAVORITELIST_H
