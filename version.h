#ifndef VERSION_H
#define VERSION_H

struct Version
{
    int major;
    int minor;
    int patch;
    int build;
};

#endif // VERSION_H
