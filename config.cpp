#include "config.h"

#define REGISTRY_USER_KEY   "HKEY_CURRENT_USER\\Software\\G2O"

Config::Config() :
    m_Registry(REGISTRY_USER_KEY, QSettings::NativeFormat)
{

}

Config& Config::get()
{
    static Config instance;
    return instance;
}

void Config::setNickname(QString nickname)
{
    // Convert unicode to the ASCII
    m_Registry.setValue("nickname", nickname.toStdString().c_str());
}

void Config::setWorld(QString world)
{
    m_Registry.setValue("world", world);
}

void Config::setAddress(ServerAddress address)
{
    m_Registry.setValue("ip_port", address.IP + ":" + QString::number(address.Port));
}

void Config::setLang(QString lang)
{
    m_Registry.setValue("lang", lang);
}

QString Config::getNickname()
{
    return m_Registry.value("nickname", "Nickname").toString();
}

QString Config::getWorld()
{
    return m_Registry.value("world", "NEWWORLD\\NEWWORLD.ZEN").toString();
}

ServerAddress Config::getAddress()
{
    QStringList ipAddress = m_Registry.value("ip_port", "127.0.0.1:28970").toString().split(':');
    return ServerAddress { ipAddress.at(0), ipAddress.at(0).toUShort() };
}

QString Config::getLang()
{
    return m_Registry.value("lang", "en").toString();
}
