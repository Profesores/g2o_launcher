# Gothic 2 Online - Launcher
Official launcher for modification Gothic 2 Online.

### Todos
  - Options
  - Unit tests

### Technology
Launcher is written in Qt 5.9.1 and uses a number of open source projects to work properly:

  - [zlib 1.2.11](https://zlib.net/) - popular compression library
  - [QuaZip 0.7.3](http://quazip.sourceforge.net/) - a simple C++ Qt wrapper over Gilles Vollant's ZIP/UNZIP package that can be used to access ZIP archives

License
----
The code is licensed under **GNU GENERAL PUBLIC LICENSE**.

Logo was created by **AlbatrOS**, and he holds all the rights.