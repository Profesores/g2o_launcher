#include <QStringList>

#include "serveraddress.h"

ServerAddress ServerAddress::parse(QString ipAddr)
{
    QStringList data = ipAddr.split(':');
    if (data.size() != 2) return ServerAddress();

    bool converted = false;

    ServerAddress address;
    address.IP = data.at(0);
    address.Port = data.at(1).toUShort(&converted);

    return converted ? address : ServerAddress();
}

bool ServerAddress::isUnassigned()
{
    return IP.isEmpty();
}
