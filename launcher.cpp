#include <QPixmap>
#include <QPalette>
#include <QDir>
#include <QMessageBox>

#include "launcher.h"
#include "proxy.h"
#include "servermanager.h"
#include "config.h"
#include "ui_launcher.h"

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Launcher),
    m_NetList(ui),
    m_FavList(ui)
{
    ui->setupUi(this);
    QDir::setCurrent(qApp->applicationDirPath());

    setWindowTitle("Gothic 2 Online - Launcher");
    setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMinimizeButtonHint);
    setFixedSize(size());

    // Register as meta type
    qRegisterMetaType<PacketPing>("PacketPing");
    qRegisterMetaType<PacketInfo>("PacketInfo");
    qRegisterMetaType<PacketDetails>("PacketDetails");

    init();
    m_NetList.init();
    m_FavList.init();

    m_NetList.refresh();

    loadConfig();
    loadStyles(":/resources/style.css");

    // Check for updates
    m_Updater.check();
}

Launcher::~Launcher()
{
    Config::get().setNickname(ui->editNickname->text());
    delete ui;
}

void Launcher::init()
{
    Version version = Proxy::version();
    QString versionText = QString("Version: %1.%2.%3.%4")
            .arg(version.major)
            .arg(version.minor)
            .arg(version.patch)
            .arg(version.build);

    ui->labVersion->setText(versionText);

    // Connections
    connect(ui->btnRefresh, SIGNAL(released()), SLOT(onRefresh()));
}

void Launcher::loadConfig()
{
    ui->editNickname->setText(Config::get().getNickname());
}

void Launcher::loadStyles(QString filePath)
{
    QPixmap bg(":/resources/background-min.jpg");
    bg = bg.scaled(size(), Qt::IgnoreAspectRatio);

    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    setPalette(palette);

    QFile styles(filePath);
    if (!styles.open(QFile::ReadOnly))
        return;

    QString styleSheet = QLatin1String(styles.readAll());
    styles.close();

    qApp->setStyleSheet(styleSheet);
    update();
}

void Launcher::onRefresh()
{
    switch (ui->tabServers->currentIndex())
    {
    case 0: // Internet
        m_NetList.refresh();
        break;

    case 1: // Favorite
        m_FavList.refresh();
        break;
    }
}
