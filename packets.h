#ifndef PACKETS_H
#define PACKETS_H

#include "serveraddress.h"
#include "version.h"

#define PACKET_VERSION 0x1

class Packet
{
public:
    ServerAddress address;
};

class PacketPing : public Packet
{
public:
    static PacketPing parse(ServerAddress &address, char *buffer, int len);

    int ping;
};

class PacketInfo : public Packet
{
public:
    static PacketInfo parse(ServerAddress &address, char *buffer, int len);
    static PacketInfo empty(ServerAddress &address);

    int ping;
    Version version;
    int players;
    int maxSlots;
    QString hostName;
};

class PacketDetails : public Packet
{
public:
    static PacketDetails parse(ServerAddress &address, char *buffer, int len);

    QString world;
    QString description;
};

#endif // PACKETS_H
