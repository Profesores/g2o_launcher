#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

class DownloadManager : public QObject
{
    Q_OBJECT

public:
    explicit DownloadManager(QObject *parent = nullptr);
    void download(QUrl url);

signals:
    void finished(QUrl url, QByteArray data);
    void progress(qint64 current, qint64 max);

private slots:
    void onDownloadFinished(QNetworkReply *pReply);
    void onDownloadProgress(qint64 current, qint64 max);

private:
    QNetworkAccessManager m_Manager;
    QList<QNetworkReply*> m_Downloads;
};

#endif // DOWNLOADMANAGER_H
