#include <QMessageBox>

#include "autoupdater.h"
#include "proxy.h"
#include "ui_autoupdater.h"

AutoUpdater::AutoUpdater(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AutoUpdater)
{
    ui->setupUi(this);

    m_pUpdateTimer = new QTimer(this);
    m_pUpdateTimer->setInterval(100 * 120); // Check update every 2 minutes
    m_pUpdateTimer->setSingleShot(true);

    connect(m_pUpdateTimer, SIGNAL(timeout()), SLOT(check()));
    connect(&m_Http, SIGNAL(finished(QUrl,QJsonDocument)), SLOT(onRequestFinished(QUrl,QJsonDocument)));
    connect(&m_Download, SIGNAL(finished(QUrl,QByteArray)), SLOT(onDownloadFinished(QUrl,QByteArray)));
    connect(&m_Download, SIGNAL(progress(qint64,qint64)), SLOT(onDownloadProgress(qint64,qint64)));
}

AutoUpdater::~AutoUpdater()
{
    delete ui;
}

void AutoUpdater::check()
{
    Version version = Proxy::version();

    QJsonObject jsonVersion;
    jsonVersion["Major"] = version.major;
    jsonVersion["Minor"] = version.minor;
    jsonVersion["Patch"] = version.patch;
    jsonVersion["Build"] = version.build;

    QJsonDocument doc(jsonVersion);
    m_Http.post(QUrl("http://api.gothic-online.com.pl/update/version/"), doc);
}

void AutoUpdater::versionOutdated(QJsonObject obj)
{
    auto itLink = obj.find("link");
    if (itLink == obj.end()) return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Updater", "Update to the newest version?", QMessageBox::Yes | QMessageBox::No);

    if (reply == QMessageBox::Yes) {
        m_Download.download(QUrl((*itLink).toString()));
        ui->progBar->setValue(0);
        m_pUpdateTimer->start();

        show();
    }
}

void AutoUpdater::onRequestFinished(QUrl url, QJsonDocument doc)
{
    QJsonObject jsonVersion = doc.object();

    // Code
    auto itCode = jsonVersion.find("code");
    if (itCode == jsonVersion.end()) return;

    int code = (*itCode).toInt();

    switch (code)
    {
    case VersionCode::UP_TO_DATE:
        m_pUpdateTimer->start();
        break;

    case VersionCode::OUTDATED:
        versionOutdated(jsonVersion);
        break;

    case VersionCode::NOT_SUPPORTED:
        QMessageBox::warning(this, "Updater", "This version is not supported any more! "
                             "Please visit our <a href='http://gothic-online.com.pl'>website</a> to download the newest version.");
        break;
    }
}

void AutoUpdater::onDownloadFinished(QUrl url, QByteArray data)
{
    hide();

    UpdatePack pack(data);
    pack.extract();

    UpdateScript script(pack.files());
    script.execute();
}

void AutoUpdater::onDownloadProgress(qint64 current, qint64 max)
{
    ui->progBar->setValue(current * 100 / max);
}
