#ifndef AUTOUPDATER_H
#define AUTOUPDATER_H

#include <QWidget>
#include <QFile>
#include <QBuffer>
#include <QTimer>

#include "httpmanager.h"
#include "downloadmanager.h"
#include "updatepack.h"
#include "updatescript.h"

namespace Ui {
class AutoUpdater;
}

class AutoUpdater : public QWidget
{
    Q_OBJECT

public:
    explicit AutoUpdater(QWidget *parent = 0);
    ~AutoUpdater();

public slots:
    void check();

private slots:
    void onRequestFinished(QUrl url, QJsonDocument doc);
    void onDownloadFinished(QUrl url, QByteArray data);
    void onDownloadProgress(qint64 current, qint64 max);

private:
    enum VersionCode
    {
        UP_TO_DATE,
        OUTDATED,
        NOT_SUPPORTED,
        ERROR
    };

    // Response events
    void versionOutdated(QJsonObject obj);

    Ui::AutoUpdater *ui;

    QTimer *m_pUpdateTimer;
    HttpManager m_Http;
    DownloadManager m_Download;
};

#endif // AUTOUPDATER_H
