#ifndef PROXY_H
#define PROXY_H

#include "version.h"

#define PROXY "G2O_Proxy.dll"

namespace Proxy {
    enum class Error
    {
        OK,
        MISSING_VERSION,
        GOTHIC_NOT_FOUND,
        UNKNOWN
    };

    struct RunResult
    {
        Error result;
        int error;
    };

    Version version();
    void run(int major, int minor, int patch);
}

#endif // PROXY_H
