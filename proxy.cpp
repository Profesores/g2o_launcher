#include <Windows.h>
#include <QMessageBox>

#include "proxy.h"

static QString possibleSolution(int code)
{
    switch (code)
    {
    case 5:
        return "<a href=\"http://dl1.worldofplayers.ru/games/gothic/patches/G2/gothic2_fix-2.6.0.0-rev2.exe\">Gothic Fix 2.6-rev (fix)</a> is not installed";

    case 126:
        return "Missing <a href=\"https://support.microsoft.com/pl-pl/help/2999226/update-for-universal-c-runtime-in-windows\">C Runtime Library</a>";

    case 193:
        return "<a href=\"https://www.microsoft.com/pl-pl/download/details.aspx?id=48145\">Visual Redistribute Patch 2015 (x86)</a> is not installed."
               "But if you already installed it, use \"Repair\" option.";
    }

    return "Unknown";
}

namespace Proxy {
    typedef Version(*FUNC_Version)();
    typedef RunResult(*FUNC_Run)(int, int, int);

    Version version()
    {
        HMODULE hProxy = LoadLibraryA(PROXY);
        if (hProxy == nullptr)
        {
            QMessageBox::warning(nullptr, "Proxy", QString("Cannot load G2O_Proxy.dll!\nError code: %1").arg(GetLastError()));
            return Version({0, 0, 0, 0});
        }

        FUNC_Version G2O_Version = (FUNC_Version)GetProcAddress(hProxy, "G2O_Version");
        if (G2O_Version == nullptr)
        {
            QMessageBox::warning(nullptr, "Proxy", QString("Cannot find G2O_Version!\nError code: %1").arg(GetLastError()));
            return Version({0, 0, 0, 0});
        }

        Version version = G2O_Version();
        FreeLibrary(hProxy);

        return version;
    }

    void run(int major, int minor, int patch)
    {
        HMODULE hProxy = LoadLibraryA(PROXY);
        if (hProxy == nullptr)
        {
            QMessageBox::warning(nullptr, "Proxy", QString("Cannot load G2O_Proxy.dll!\nError code: %1").arg(GetLastError()));
            return;
        }

        FUNC_Run G2O_Run = (FUNC_Run)GetProcAddress(hProxy, "G2O_Run");
        if (G2O_Run == nullptr)
        {
            QMessageBox::warning(nullptr, "Proxy", QString("Cannot find G2O_Run!\nError code: %1").arg(GetLastError()));
            return;
        }

        RunResult result = G2O_Run(major, minor, patch);
        FreeLibrary(hProxy);

        if (result.result != Error::OK)
        {
            QString errorMsg;
            switch (result.result)
            {
            case Error::MISSING_VERSION:
                errorMsg = QString("You can't join to this server, because you don't have required version!\nCode: %1").arg(result.error);
                break;

            case Error::GOTHIC_NOT_FOUND:
                errorMsg = QString("Could not open Gothic2.exe.<br>Did you install G2O to, a folder with Gothic 2: Night of the Raven?<br>Try to run launcher with admin rights.<br>Code: <b>%1</b>").arg(result.error);
                break;

            case Error::UNKNOWN:
                errorMsg = QString("Possible solution: %1\nCode: %2").arg(possibleSolution(result.error)).arg(result.error);
                break;
            }

            QMessageBox::warning(nullptr, "Proxy", errorMsg);
        }
    }
}
