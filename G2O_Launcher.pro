#-------------------------------------------------
#
# Project created by QtCreator 2017-08-09T20:39:32
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = G2O_Launcher
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
    CONFIG += embed_manifest_exe
    QMAKE_LFLAGS += \"/MANIFESTUAC:level=\'requireAdministrator\' uiAccess=\'false\'\"
}

win32:RC_ICONS += go_icon.ico

SOURCES += \
        main.cpp \
        launcher.cpp \
    serverlist.cpp \
    downloadmanager.cpp \
    httpmanager.cpp \
    autoupdater.cpp \
    updatepack.cpp \
    updatescript.cpp \
    proxy.cpp \
    config.cpp \
    servermanager.cpp \
    serveraddress.cpp \
    packets.cpp \
    servertreewidget.cpp \
    favoritelist.cpp \
    internetlist.cpp

HEADERS += \
        launcher.h \
    serverlist.h \
    pch.h \
    downloadmanager.h \
    httpmanager.h \
    autoupdater.h \
    updatepack.h \
    updatescript.h \
    proxy.h \
    version.h \
    config.h \
    servermanager.h \
    serveraddress.h \
    packets.h \
    servertreewidget.h \
    favoritelist.h \
    internetlist.h

FORMS += \
        launcher.ui \
    autoupdater.ui

win32 {
    LIBS += ws2_32.lib

    LIBS += -L$$PWD/quazip/lib/ -lquazip

    INCLUDEPATH += $$PWD/quazip/include
    DEPENDPATH += $$PWD/quazip/include

    LIBS += -L$$PWD/zlib/lib/ -lzlib

    INCLUDEPATH += $$PWD/zlib/include
    DEPENDPATH += $$PWD/zlib/include
}

RESOURCES += \
    resources.qrc
