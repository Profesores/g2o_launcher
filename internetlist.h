#ifndef INTERNETLIST_H
#define INTERNETLIST_H

#include "serverlist.h"
#include "httpmanager.h"

class InternetList : public ServerList
{
    Q_OBJECT

public:
    InternetList(Ui::Launcher *ui);
    void init();
    void refresh();

private slots:
    void onServerInfo(PacketInfo packet) override;
    void onRequestFinished(QUrl url, QJsonDocument doc);

private:
    HttpManager m_Http;
};

#endif // INTERNETLIST_H
