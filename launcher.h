#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>
#include <QTimer>

#include "autoupdater.h"
#include "internetlist.h"
#include "favoritelist.h"

namespace Ui {
class Launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = 0);
    ~Launcher();

private slots:
    void onRefresh();

private:
    void init();
    void loadConfig();
    void loadStyles(QString filePath);

    Ui::Launcher *ui;
    AutoUpdater m_Updater;
    InternetList m_NetList;
    FavoriteList m_FavList;
};

#endif // LAUNCHER_H
