#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include <QObject>
#include <QMutex>
#include <QThread>
#include <QVector>
#include <QDate>
#include <QDateTime>
#include <WinSock2.h>

#include "serveraddress.h"
#include "packets.h"

class ServerManager : public QObject
{
    Q_OBJECT

public:
    explicit ServerManager(QObject *parent = nullptr);
    ~ServerManager();

    bool start(QThread &thread);
    void stop();

    void clear();
    QVector<ServerAddress> failRequests();
    void info(ServerAddress addres);
    void ping(ServerAddress addres);
    void details(ServerAddress addres);

signals:
    void serverPing(PacketPing packet);
    void serverInfo(PacketInfo packet);
    void serverDetails(PacketDetails packet);

private slots:
    void listener();

private:
    struct Request
    {
        ServerAddress address;
        QString packet;
        qint64 time = -1;

        bool isNull() { return time == -1; }
    };

    void send(ServerAddress &addres, QString packet);
    qint64 exists(ServerAddress &address, QString packet);
    bool initWinSock();

    SOCKET m_Socket;
    QMutex m_Mutex;
    QVector<Request> m_Requests;
    bool m_Running = false;
};

#endif // SERVERMANAGER_H
