#include "internetlist.h"
#include "ui_launcher.h"

InternetList::InternetList(Ui::Launcher *ui) :
    ServerList(ui)
{

}


void InternetList::init()
{
    // Remove margins so it looks much better
    pUi->tabInternet->layout()->setMargin(0);
    pUi->treeInternet->init();

    // Connections
    connect(&m_Manager, SIGNAL(serverInfo(PacketInfo)), SLOT(onServerInfo(PacketInfo)));
    connect(&m_Manager, SIGNAL(serverDetails(PacketDetails)), SLOT(onServerDetails(PacketDetails)));
    connect(&m_Http, SIGNAL(finished(QUrl,QJsonDocument)), SLOT(onRequestFinished(QUrl,QJsonDocument)));
    connect(pUi->treeInternet, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(onServerClicked(QTreeWidgetItem*,int)));
    connect(pUi->treeInternet, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(onServerDoubleClicked(QTreeWidgetItem*,int)));
    connect(m_pTimer, SIGNAL(timeout()), SLOT(onTimeout()));
}

void InternetList::refresh()
{
    pUi->treeInternet->clear();
    m_Http.get(QUrl("http://api.gothic-online.com.pl/master/public_servers/"));
}

void InternetList::onServerInfo(PacketInfo packet)
{
    pUi->treeInternet->insert(packet);
}

void InternetList::onRequestFinished(QUrl url, QJsonDocument doc)
{
    QVector<ServerAddress> servers;
    QJsonArray jsonServers = doc.array();

    for (auto jsonServer : jsonServers)
    {
        QJsonObject obj = jsonServer.toObject();

        auto itIp = obj.find("ip");
        if (itIp == obj.end()) continue;

        auto itPort = obj.find("port");
        if (itPort == obj.end()) continue;

        servers.append({(*itIp).toString(), static_cast<ushort>((*itPort).toInt())});
    }

    ServerList::refresh(servers);
}
