#include "servertreewidget.h"

ServerTreeWidget::ServerTreeWidget(QWidget *parent) :
    QTreeWidget(parent)
{
}

void ServerTreeWidget::init()
{
    QStringList labels;
    labels << "Hostname" << "IP" << "Port" << "Version" << "Players" << "Ping";
    setHeaderLabels(labels);

    setContextMenuPolicy(Qt::CustomContextMenu);
    setColumnWidth(0, 200);
    setColumnWidth(2, 50);
    setColumnWidth(3, 50);
}

void ServerTreeWidget::insert(PacketInfo &packet)
{
    QString version = QString("%1.%2.%3")
            .arg(packet.version.major)
            .arg(packet.version.minor)
            .arg(packet.version.patch);

    QTreeWidgetItem *pItem = new QTreeWidgetItem(this);
    pItem->setText(0, packet.hostName);
    pItem->setText(1, packet.address.IP);
    pItem->setText(2, QString::number(packet.address.Port));
    pItem->setText(3, version);
    pItem->setText(4, QString("%1/%2").arg(packet.players).arg(packet.maxSlots));
    pItem->setText(5, QString::number(packet.ping));

    this->addTopLevelItem(pItem);
}
